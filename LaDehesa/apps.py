from django.apps import AppConfig


class LadehesaConfig(AppConfig):
    name = 'LaDehesa'
