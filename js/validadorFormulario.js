$("#formulario1").validate(
{
    rules:{
        "txtEmail":{
            required:true,
            email:true
        },
        "txtRun":{
            required:true,
            minlength:5,
            maxlength:10
        },
        "txtNombre":{
            required:true,
            minlength:10,
            maxlength:60
        },
        "txtFechaNacimiento":{
            required:true
        },
        "txtTelefonoContacto":{
            required:true,
            minlength:12,
            maxlength:12
        },
        "regiones":{
            required:true
        },
        "comunas":{
            required:true
        }
    },
    messages:{
        "txtEmail":{
            required:"Debes ingresar un email.",
            email:"Debes ingresar un email válido."
        },
        "txtRun":{
            required:"Debes ingresar un run.",
            minlength:"Debes ingresar un rut correcto.",
            maxlength:"El rut es demasiado largo."
        },
        "txtNombre":{
            required:"Debes ingresar un nombre.",
            minlength:"El nombre es demasiado corto.",
            maxlength:"El nombre es demasiado largo."
        },
        "txtFechaNacimiento":{
            required:"Debes seleccionar una fecha de nacimiento."
        },
        "txtTelefonoContacto":{
            required:"Debes ingresar un telefono.",
            minlength:"Telefono incorrecto",
            maxlength:"Telefono incorrecto"
        },
        "regiones":{
            required:"Debes seleccionar una region."
        },
        "comunas":{
            required:"Debes seleccionar una comuna."
        }
    }
}
);

var Fn = {
	// Valida el rut con su cadena completa "XXXXXXXX-X"
	validaRut : function (rutCompleto) {
		rutCompleto = rutCompleto.replace("‐","-");
		if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( rutCompleto ))
			return false;
		var tmp 	= rutCompleto.split('-');
		var digv	= tmp[1]; 
		var rut 	= tmp[0];
		if ( digv == 'K' ) digv = 'k' ;
		
		return (Fn.dv(rut) == digv );
	},
	dv : function(T){
		var M=0,S=1;
		for(;T;T=Math.floor(T/10))
			S=(S+T%10*(9-M++%6))%11;
		return S?S-1:'k';
	}
}


$("#btnValida").click(function(){
	if (Fn.validaRut( $("#txtRun").val() )){
		alert("rut valido");
	} else {
		alert("¡ Rut inválido ! \n Por favor ingrese un rut válido.")
	}
});
